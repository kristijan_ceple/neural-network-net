﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tut_AI_Genetic_NN
{
    internal static class DataLoader
    {
        internal class LoadedData
        {
            public List<string> headerLabels { get; set; } = new List<string>();
            public List<KeyValuePair<double, double>> data { get; set; } = new List<KeyValuePair<double, double>>();
            public bool dataLoaded { get; set; } = false;
        }

        public static LoadedData loadTrainData(string fileName)
        {
            LoadedData loadedData = new LoadedData();

            if(File.Exists(fileName)) {
                // Open the file and populate the List
                // First line contains the header data
                // Other lines contain the actual data in "x y" format
                using (StreamReader myFileStream = new StreamReader(fileName)) {
                    string currLine;

                    currLine = myFileStream.ReadLine();
                    string[] currLineParts = currLine.Split(',');
                    loadedData.headerLabels.AddRange(currLineParts);

                    // Move on the data itself
                    double xVal, yVal;
                    while((currLine = myFileStream.ReadLine()) != null) {
                        currLineParts = currLine.Split(',');
                        xVal = double.Parse(currLineParts[0]);
                        yVal = double.Parse(currLineParts[1]);

                        KeyValuePair<double, double> currDataPair = new KeyValuePair<double, double>(xVal, yVal);
                        loadedData.data.Add(currDataPair);
                    }
                }

                loadedData.dataLoaded = true;
            } else {
                Console.Error.WriteLine("File not found!");
            }

            return loadedData;
        }
    }
}
