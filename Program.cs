﻿using Tut_AI_Genetic_NN;

namespace Tut_AI_Genetic_NN
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1) {
                Console.Error.WriteLine("Please pass the required arguments!");
                return;
            }

            string inputDataPath = args[0];

            var loadedData = DataLoader.loadTrainData(inputDataPath);
            if (loadedData.dataLoaded == false) {
                Console.Error.WriteLine("Data not loaded!");
                return;
            }

            Console.WriteLine($"Loaded data from: {inputDataPath}");
            Console.WriteLine($"Header labels: {string.Join(",", loadedData.headerLabels)}");

            Console.WriteLine("Loaded data:");
            foreach (KeyValuePair<double, double> currKeyValPair in loadedData.data) {
                Console.WriteLine($"x: {currKeyValPair.Key}, y: {currKeyValPair.Value}");
            }

            // Excellent, let's move on
            Console.WriteLine("Creating a Neural Network...");

            List<int> hiddenLayerDims = new List<int> { 5, 5, 5 };
            List<Func<double, double>> transferFunctions = new List<Func<double, double>> { Util.sigmoidTransferFunction, Util.sigmoidTransferFunction, Util.sigmoidTransferFunction };

            NeuralNetwork myNeuralNetwork = new NeuralNetwork(hiddenLayerDims, transferFunctions);
            myNeuralNetwork.run(3);
        }
    }
}