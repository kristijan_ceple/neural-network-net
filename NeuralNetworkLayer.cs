﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Tut_AI_Genetic_NN
{
    internal class NeuralNetworkLayer
    {
        public List<Neuron> NeuronsList { get; private set; } = new List<Neuron>();
        public int LayerIndex { get; private set; } = 0;
        public event EventHandler<LayerWeightInputsEventArgs> workCompleted;        // The next layer will subscribe to this event, and when it is invoked it will calculate its own layer
        private Barrier layersCompStartBarrier = null;
        public Func<double, double> OutputTransferFunction { get; private set; } = Util.sigmoidTransferFunction;
        public int CurrFFIterations { get; internal set; }
        private bool dataReceived = false;
        public NeuralNetworkLayer NextLayer { get; set; }  = null;

        public ManualResetEvent prevLayerSendMRE { get; set; } = null;
        public ManualResetEvent prevLayerReceiveMRE { get; set; } = null;
        public ManualResetEvent nextLayerSendMRE { get; set; } = null;
        public ManualResetEvent nextLayerReceiveMRE { get; set; } = null;

        public bool lastLayer { get; set; } = false;
        public List<double> bufferPrevLayerWeights { get; set; } = new List<double>();
        public List<double> bufferNextLayerWeights { get; set; } = new List<double>();
        public List<double> currLayerWeights { get; set; } = new List<double>();

        public NeuralNetworkLayer(int layerIndex, int layerDimension, int prevLayerDimension, Func<double, double> outputTransferFunction, Barrier layersCompStartBarrier) {
            if (layerIndex < 0 || layerDimension < 1) {
                throw new ArgumentException("ERROR INITIALISING NEURAL NETWORK LAYER: Layer index is less than 0 or layer dimension is less than 1");
            }
            
            this.LayerIndex = layerIndex;
            this.OutputTransferFunction = outputTransferFunction;
            this.layersCompStartBarrier = layersCompStartBarrier;

            for(int i = 0; i < layerDimension; i++) {
                this.NeuronsList.Add(new Neuron(this.OutputTransferFunction, prevLayerDimension));
            }
        }
        
        public void run()
        {
            Console.WriteLine($"Neural Network Layer {this.LayerIndex} started!");

            // First we must wait for everyone to cross the barrier
            this.layersCompStartBarrier.SignalAndWait();
            Console.WriteLine($"Neural Network Layer {this.LayerIndex} has crossed the Barrier!");

            // Here begins the algorithm
            for (int i = 0; i < this.CurrFFIterations; i++) {
                Console.WriteLine($"Neural Network Layer {this.LayerIndex} is at iteration {i}!");

                // Wait until we receive our data
                // If we're the first index we ain't receiving data from anyone, just continually generating it
                if (this.LayerIndex != 0) {
                    // We will know we received our data via the event
                    this.prevLayerReceiveMRE.WaitOne();

                    // Take the data
                    this.currLayerWeights = this.bufferPrevLayerWeights;

                    // Tell them we've taken it
                    this.prevLayerSendMRE.Set();
                }

                // Update weights for each neuron
                foreach(Neuron currNeuron in this.NeuronsList) {
                    currNeuron.NeuronInputs.Clear();
                    currNeuron.NeuronInputs.AddRange(this.currLayerWeights);
                }

                // Calculate
                List<double> currLayerOutputs = new List<double>();
                foreach (Neuron currNeuron in this.NeuronsList) {
                    double tmpRes = currNeuron.calculateNetAndTransfer();
                    currLayerOutputs.Add(tmpRes);
                }

                // Send data to the next layer
                if(this.lastLayer == false) {
                    // Set the data
                    this.bufferNextLayerWeights = currLayerOutputs;

                    // Wake up the next layer from sleep
                    this.nextLayerSendMRE.Set();

                    // Wait until it has taken the data
                    this.nextLayerReceiveMRE.WaitOne();
                }
            }
        }

        // Delegate subscribers to prev layer events
        public static void prevLayerCompletedWork(object sender, LayerWeightInputsEventArgs eventArgs)
        {
            // Previous layer has completed its computation
            // Compute data for our layer, and then notify the layer in front of us
            NeuralNetworkLayer prevLayer = (NeuralNetworkLayer)sender;
            Console.WriteLine($"Layer index {prevLayer.LayerIndex + 1} received event!");

            List<double> prevLayerOutputs = eventArgs.PrevLayerWeights;
            NeuralNetworkLayer nextLayer = eventArgs.Receiver;

            // TODO: Set the weights of the Neurons here
            // blablabla

            // Tell the next Layer that the weights have been set and that it can proceed to the computations
            nextLayer.dataReceived = true;
        }

        public class LayerWeightInputsEventArgs : EventArgs
        {
            public List<double> PrevLayerWeights { get; private set; } = new List<double>();
            public NeuralNetworkLayer Sender { get; private set; }
            public NeuralNetworkLayer Receiver { get; private set; }

            public LayerWeightInputsEventArgs(List<double> prevLayerWeights, NeuralNetworkLayer senderLayer, NeuralNetworkLayer receiverLayer)
            {
                this.PrevLayerWeights = prevLayerWeights;
                this.Sender = senderLayer;
                this.Receiver = receiverLayer;
            }
        }
    }
}
