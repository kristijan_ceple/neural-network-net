﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tut_AI_Genetic_NN
{
    internal static class Util
    {
        public const string SINE_TRAIN_FILE = "sine_train.txt";
        public const string SINE_TEST_FILE = "sine_test.txt";

        public const string ROSENBROCK_TRAIN_FILE = "rosenbrock_train.txt";
        public const string ROSENBROCK_TEST_FILE = "rosenbrock_test.txt";

        public const string RASTRIGIN_TRAIN_FILE = "rastrigin_train.txt";
        public const string RASTRIGIN_TEST_FILE = "rastrigin_test.txt";

        public static double sigmoidTransferFunction(double input)
        {
            double res;

            res = Math.Pow(Math.E, -1 * input) + 1;
            res = 1.0 / res;

            return res;
        }

        public static double meanSquareError(List<double> calculatedResults, List<double> correctResults)
        {
            double res = 0.0;
            double tmp;

            foreach(double diff in calculatedResults.Zip(correctResults, (currCalcRes, currCorrRes) => currCorrRes - currCalcRes)) {
                tmp = diff*diff;
                res += tmp;
            }

            res /= correctResults.Count;

            return res;
        }
    }
}
