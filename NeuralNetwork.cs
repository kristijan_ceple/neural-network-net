﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tut_AI_Genetic_NN
{
    internal class NeuralNetwork
    {
        public const int ITERATIONS_DEFAULT = 1;

        public List<int> hiddenLayerDimensions { get; private set; } = new List<int>();
        public List<Func<double, double>> transferFunctions { get; private set; } = new List<Func<double, double>>();
        public int hiddenLayersNum { get; private set; }  = 0;
        private List<NeuralNetworkLayer> hiddenLayers = new List<NeuralNetworkLayer>();
        private Barrier layersCompStartBarrier = null;
        // First is used for left-signals-right, and the second one is used for right-signals-left
        public List<Tuple<ManualResetEvent, ManualResetEvent>> ThreadSignals { get; private set; } = new List<Tuple<ManualResetEvent, ManualResetEvent>>();

        public bool destructed { get; private set; } = false;

        public NeuralNetwork(List<int> layerDimensions, List<Func<double, double>> transferFunctions) {
            this.hiddenLayerDimensions.AddRange(layerDimensions);
            this.hiddenLayersNum = this.hiddenLayerDimensions.Count;
            this.layersCompStartBarrier = new Barrier(this.hiddenLayersNum);

            this.transferFunctions.AddRange(transferFunctions);

            NeuralNetworkLayer prevLayer = null;

            if (this.hiddenLayersNum >= 1) {
                // Add the first layer
                prevLayer = new NeuralNetworkLayer(0, this.hiddenLayerDimensions[0], 0, this.transferFunctions[0], this.layersCompStartBarrier);
                
                if(this.hiddenLayersNum == 1) {
                    prevLayer.lastLayer = true;
                }

                this.hiddenLayers.Add(prevLayer);
            } else {
                throw new ArgumentException("Neural Network hasn't got a single layer?!?");
            }

            for (int i = 1; i < this.hiddenLayersNum; i++) {
                NeuralNetworkLayer currLayer = new NeuralNetworkLayer(i, this.hiddenLayerDimensions[i], this.hiddenLayerDimensions[i-1], this.transferFunctions[i], this.layersCompStartBarrier);

                // Add it
                this.hiddenLayers.Add(currLayer);

                // Create and assign the multithreaded data objects
                ManualResetEvent prevLayerSendMRE = new ManualResetEvent(false);
                ManualResetEvent prevLayerReceiveMRE = new ManualResetEvent(false);

                prevLayer.nextLayerSendMRE = prevLayerReceiveMRE;
                prevLayer.nextLayerReceiveMRE = prevLayerSendMRE;
                currLayer.prevLayerSendMRE = prevLayerSendMRE;
                currLayer.prevLayerReceiveMRE = prevLayerReceiveMRE;

                // Assign buffers between layers
                List<double> bufferWeights = new List<double>();
                prevLayer.bufferNextLayerWeights = bufferWeights;
                currLayer.bufferPrevLayerWeights = bufferWeights;

                // Set the 'last' field
                if(i == this.hiddenLayersNum-1) {
                    currLayer.lastLayer = true;
                }

                // Move the previous layer up
                prevLayer = currLayer;
            }
        }

        public void run(int iterations = NeuralNetwork.ITERATIONS_DEFAULT)
        {
            // Okay, let's create and start the neural network layers - will have to take care of the input and output layers separately somehow
            // Start the threads
            List<Thread> neuralNetworkLayersThreads = new List<Thread>();
            for(int i = 0; i < this.hiddenLayersNum; i++) {
                NeuralNetworkLayer currLayer = this.hiddenLayers[i];
                currLayer.CurrFFIterations = iterations;
                Thread currLayerThread = new Thread(currLayer.run);
                neuralNetworkLayersThreads.Add(currLayerThread);
            }

            // Start them all
            foreach(Thread currLayerThread in neuralNetworkLayersThreads) {
                currLayerThread.Start();
            }

            // Join them all
            foreach(Thread currLayerThread in neuralNetworkLayersThreads) {
                currLayerThread.Join();
            }

            Console.WriteLine("All calculations successfully completed");

        }

        public void destruct() {
            if(this.destructed != true) {
                this.hiddenLayerDimensions = null;
                this.hiddenLayers = null;
                this.hiddenLayersNum = 0;

                this.transferFunctions = null;
                
                this.destructed = true;
            }
        }
    }
}
