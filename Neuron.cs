﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Distributions;

namespace Tut_AI_Genetic_NN
{
    internal class Neuron
    {
        public const double DEFAULT_WEIGHT = 0.0;
        public const double STANDARD_DEVIATION_WEIGHTS_NORMAL_DISTR_DEFAULT = 0.01;
        public const double MEAN_WEIGHTS_NORMAL_DISTR_DEFAULT = 0.0;

        public double Bias { get; private set; }
        public List<double> Weights { get; private set; } = new List<double>();         // TODO: Replace this with NumSharp??
        public List<double> NeuronInputs { get; private set; } = new List<double>();    // TODO: Replace this with NumSharp??
        public double NeuronOutput { get; private set; } = 0.0;

        public bool Destructed { get; private set; } = false;

        public Func<double, double> NeuronOutputTransferFunction { get; private set; } = null;

        public Neuron(Func<double, double> neuronOutputTransferFunction, int weightsNum = 0)
        {
            if (weightsNum < 0) {
                throw new ArgumentException("Negative amount of input weights been specified for this Neuron!");
            }

            // Fill the weights with values sampled from a normal distribution with standard deviation of 0.01
            var normalDist = new Normal(Neuron.MEAN_WEIGHTS_NORMAL_DISTR_DEFAULT, Neuron.STANDARD_DEVIATION_WEIGHTS_NORMAL_DISTR_DEFAULT);
            this.Bias = normalDist.Sample();

            for (int i = 0; i < weightsNum; i++) {
                this.Weights.Add(normalDist.Sample());
            }

            // Initialise the inputs here
            for(int i = 0; i < this.Weights.Count; i++) {
                this.NeuronInputs.Add(0.0);
            }

            // Prepare the transfer function
            this.NeuronOutputTransferFunction = neuronOutputTransferFunction;
        }

        public double calculateNetAndTransfer()
        {
            double res = 0.0;

            foreach(double tmp in this.Weights.Zip(this.NeuronInputs, (currWeight, currNeuronInput) => currWeight*currNeuronInput)) {
                res += tmp;
            }

            res += this.Bias;
            res = Util.sigmoidTransferFunction(res);

            return res;
        }

        public void destruct()
        {
            this.Weights = null;

            this.Destructed = true;
        }
    }
}
